import org.apache.http.*;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.protocol.*;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.*;
import java.util.*;


/**
 * Created by lukepetruzzi
 */

// This could easily just be another key-value map, but I wanted to give precise encapsulated results
class AdData
{
    private int num_clicks;
    private int num_impressions;

    public AdData(int num_clicks, int num_impressions)
    {
        this.num_clicks = num_clicks;
        this.num_impressions = num_impressions;
    }

    // getter and setter functions for the encapsulated data
    public int getNum_clicks()
    {
        return this.num_clicks;
    }
    public void setNum_clicks(int value)
    {
        this.num_clicks = value;
    }
    public int getNum_impressions()
    {
        return this.num_impressions;
    }
    public void setNum_impressions(int value)
    {
        this.num_impressions = value;
    }

    // for testing and visualization
    public String toString()
    {
        return "num_clicks: " + num_clicks + ", num_impressions: " + num_impressions + "\n";
    }
}

public class AdvertiserDataGetter
{
    // create a synchronized list to save byte[] data from the threaded requests
    private List<byte[]> datas = Collections.synchronizedList(new LinkedList<byte[]>());

    public HashMap<String, AdData> getAdData(long[] advertiser_ids)
    {
        // create this HashMap. Keep updating it using the date as the key. This way it will add to the value
        // without worrying about which ad the data came from
        HashMap<String, AdData> aggregatedData = new HashMap<String, AdData>();

        String[] uris = new String[advertiser_ids.length];
        for (int i = 0; i < advertiser_ids.length; i++)
        {
            // craft a request for the current advertiser_id
            uris[i] = "http://dan.triplelift.net/code_test.php?advertiser_id=" + advertiser_ids[i];
        }

        multithreadedRequests(uris, advertiser_ids);

        // save the responses
        String[] responses = new String[advertiser_ids.length];

        // converts byte[] to string
        for (int i = 0; i < datas.size(); i++)
        {
            byte[] bytes = datas.get(i);

            String decoded = "";
            for (int j = 0; j < bytes.length; j++) {
                try
                {
                    // decode the result
                    decoded = decoded.concat(new String(new byte[]{bytes[j]}, "US-ASCII"));
                }catch(UnsupportedEncodingException e)
                {
                    System.out.println("UNSUPPORTED ENCODING: " + e);
                }
            }
            // add the decoded string to the responses
            responses[i] = decoded;
        }


        // now aggregate all the data we could get in 200ms
        for (int i = 0; i < responses.length; i++)
        {
            // parse the data if it was able to be retrieved in time
            if (responses[i] != null)
            {
                try {
                    JSONArray jsonData = new JSONArray(responses[i]);

                    for (int j = 0; j < jsonData.length(); j++) {
                        // get the current data entry
                        JSONObject entry = jsonData.getJSONObject(j);

                        // retrieve the relevant information
                        String ymd = entry.getString("ymd");
                        int num_clicks = entry.getInt("num_clicks");
                        int num_impressions = entry.getInt("num_impressions");

                        // encapsulate the data for the map
                        AdData current = new AdData(num_clicks, num_impressions);

                        // update with new information if there is already data for the current ymd value
                        if (aggregatedData.containsKey(ymd)) {
                            // update values
                            AdData update = aggregatedData.get(ymd);
                            int oldClicks = update.getNum_clicks();
                            int oldImpressions = update.getNum_impressions();
                            update.setNum_clicks(oldClicks + num_clicks);
                            update.setNum_impressions(oldImpressions + num_impressions);
                        }
                        else // otherwise, put in a new entry for the ymd
                        {
                            aggregatedData.put(ymd, current);
                        }
                    }
                } catch (JSONException e) {
                    System.out.println("JSON Exception: " + e.getLocalizedMessage());
                }
            }
        }

        return aggregatedData;
    }


    // using Apache HttpComponents library for multithreading requests to an open connection
    private void multithreadedRequests(String[] uris, long[] advertiser_ids)
    {
        // create a pooling connection manager
        PoolingHttpClientConnectionManager poolingConnectionMan = new PoolingHttpClientConnectionManager();
        // set the max connections on the manager to what I need
        poolingConnectionMan.setMaxTotal(uris.length);

        // create a custom HttpClient that uses my poolingConnectionManager
        CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(poolingConnectionMan).build();

        // create a thread for each URI to get
        GetThread[] threads = new GetThread[uris.length];
        for (int i = 0; i < threads.length; i++)
        {
            HttpGet httpget = new HttpGet(uris[i]);
            threads[i] = new GetThread(httpClient, httpget, advertiser_ids[i]);
        }

        // begin thread execution
        for (int j = 0; j < threads.length; j++) {
            threads[j].start();
        }

        // run the threads for 200ms each concurrently
        for (int j = 0; j < threads.length; j++) {
            try
            {
                // give each thread 200ms in parallel to run and cut it short if it can't make it
                threads[j].join(200);
            }
            catch(InterruptedException e)
            {
                System.out.println("ERROR RUNNING THREAD: " + e.getMessage());
            }
        }

        // performed all requests, can close the connection
        try
        {
            httpClient.close();
        }catch (IOException e){
            System.out.println("ERROR CLOSING HTTPCLIENT: " + e.getMessage());
        }

    }

    // this class makes a thread that does a GET request using an open connection
    private class GetThread extends Thread
    {
        private final CloseableHttpClient httpClient;
        private final HttpGet httpget;
        private final long adId;

        private final HttpContext context;

        private GetThread(CloseableHttpClient httpClient, HttpGet httpget, long adId)
        {
            this.httpClient = httpClient;
            this.httpget = httpget;
            this.adId = adId;

            this.context = new BasicHttpContext();
        }

        @Override
        public void run()
        {
            try {
                // execute the get request
                CloseableHttpResponse response = httpClient.execute(httpget, context);
                try
                {
                    // get the response body as an array of bytes
                    HttpEntity entity = response.getEntity();
                    if (entity != null)
                    {
                        // quickly save the json to parse later after the connection is closed
                        datas.add(EntityUtils.toByteArray(entity));
                    }
                }
                finally
                {
                    // close the response
                    response.close();
                }
            } catch (Exception e) {
                // for some reason, even though I'm catching the exception, another I/O Exception is also printed
                // out to the log. Can't figure out why, but it doesn't break anything
                System.out.println("NO DATA FOR ADVERTISER_ID " + adId + ". Couldn't complete request in time.");
            }
        }
    }


    // test it out!
    public static void main(String[] args)
    {
        AdvertiserDataGetter adg = new AdvertiserDataGetter();
        //long[] test = {123, 456, 789, 321, 3423423, 9894, 9303, 30};
        long[] test = new long[1000];
        for (int i = 0; i < 1000; i++)
        {
            test[i] = (long)i;
        }
        String result = adg.getAdData(test).toString();

        // wait a second so all any errors about failure to get requests in time
        // are logged before the final results
        try{
            Thread.sleep(500);
        } catch(InterruptedException e) {
            System.out.println("dumbo main exception: " + e);
        }
        System.out.println("AGGREGATED DATA:\n" + result);
    }
}
